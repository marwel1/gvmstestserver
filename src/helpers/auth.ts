import { AuthorizationCode } from 'simple-oauth2'
import dotenv from 'dotenv'
dotenv.config()

const gvmsBaseUrl: string = 'https://test-api.service.hmrc.gov.uk/'
const redirectUri: string = 'http://localhost:8080/oauth20/callback'
// const redirectUri: string = 'https://contained-server-test.herokuapp.com/oauth20/callback'
// const oauthScope: string = 'write:import-control-system' //'write:goods-movement-system'
const oauthScope: string = 'write:goods-movement-system'

// OS account
// const clientId = 'LN3CVx3dIGhxVBwq2SK854W3ZiEV'
// const clientSecret = '0bfb3435-2aff-49f2-92f9-24a1c8de3eda'

// CLEAN TEST ENV
const clientId = 'LuBzF9dOCF0cGudSTh9Vkrn5OcGl'
const clientSecret = '2565b7a1-5e56-42c9-861f-dff7aa6ccad9'

const client = new AuthorizationCode({
    client: {
      id: clientId,
      secret: clientSecret,
    },
    auth: {
      tokenHost: gvmsBaseUrl,
      tokenPath: '/oauth/token',
      authorizePath: '/oauth/authorize',
    },
})

const authorizationUri = client.authorizeURL({
    redirect_uri: redirectUri,
    scope: oauthScope
})

export {
    client,
    authorizationUri,
}