import request from 'superagent'

const BaseUrl: string = 'https://test-api.service.hmrc.gov.uk/'
const serviceNameGVMS = 'customs/goods-movement-system'
const serviceVersion = '1.0'


// endpoint: any, res: any, bearerToken: any, method = 'get', data: any
const callApi = (params:any) => {
    const acceptHeader: string = `application/vnd.hmrc.${serviceVersion}+json`
    const urlGVMS: string = BaseUrl + serviceNameGVMS + params.endpoint

    // @ts-ignore
    const reqGVMS = request
        [params.method](urlGVMS)
        .accept(acceptHeader)
        .set('Authorization', `Bearer ${params.bearerToken}`)

        if (params.method === 'post' || params.method === 'put' && params.data !== undefined) {
            reqGVMS.send(params.data)
            .then((resp: any, err: any) => {
                if(resp) {
                    let contains: object = {
                        boxId: resp.headers["notification-box-id"],
                        msgId: resp.headers["notification-message-id"],
                    }
                    params.res.send({
                        headers: resp.headers,
                        contains
                    })
                } else {
                    params.res.send(err)
                }
            })
        } else {
            reqGVMS.then((resp: any) => resp.body ? params.res.send(resp.body) : params.res.send(resp))
        }

        reqGVMS.catch((err: any) => params.res.send(err.messaage))
}

export const callApi2 = async (params:any) => {}
  
export default callApi