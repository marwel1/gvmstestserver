//DEPRECATED, KEPT AS REFERENCE

const handleResponse = (res: any, err: any, apiResponse: any) => {
    if (err || !apiResponse.ok) {
      res.send(err)
    } else {
      res.send(apiResponse.body)
      console.table(apiResponse, apiResponse.body)
    }
}

export default handleResponse