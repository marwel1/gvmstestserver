import express from 'express'
import movement from '../controllers'

const movementsRouter = express.Router()

movementsRouter.get('/', movement.routeList)
movementsRouter.get('/get-gmr-list', movement.getGmrList)
movementsRouter.get('/create-gmr', movement.createGmr)
movementsRouter.get('/get-gmr-by-id/:gmrId', movement.getGmrById)
movementsRouter.get('/update-gmr-by-id/:gmrId', movement.updateGmrById)
movementsRouter.get('/delete-gmr-by-id/:gmrId', movement.deleteGmrById)
movementsRouter.get('/get-reference-data', movement.getRefData)
movementsRouter.get('/oauth20/callback', movement.callBack)
movementsRouter.get('/push', movement.push)
movementsRouter.post('/push', movement.pull)

export default movementsRouter