import express from 'express'
import cookieSession from 'cookie-session'
import cors from 'cors'
import movementsRouter from './routes/movements'

const PORT: string|number = process.env.PORT || 8080

const app = express()

app.use(
    cookieSession({
      name: 'session',
      keys: ['oauth2Token', 'oauth2TokenAPP', 'caller'],
      maxAge: 5 * 60 * 60 * 1000, // 5 hours
    })
)
app.use(cors({
  //'Access-Control-Allow-Origin': '*',
  origin: '*',
  methods: ['GET', 'PUT', 'POST', 'PATCH', 'DELETE', 'OPTIONS'],
  allowedHeaders: ['DNT', 'X-CustomHeader', 'Keep-Alive', 'User-Agent', 'X-Requested-With', 'If-Modified-Since', 'Cache-Control', 'Content-Type', 'Content-Range', 'Range', 'Authorization'],
  credentials: true,
  optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}))
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
})

app.use('/', movementsRouter)

app.listen(PORT, () => {
    console.log('Started at http://localhost:8080')
})

// TEST HTTPS
// import https from 'https'
// import fs from 'fs'

// const server = https.createServer({
//   key: fs.readFileSync('server.key'),
//   cert: fs.readFileSync('server.cert')
// }, app)

// server.listen(PORT, () => {
//   console.log('Started at https://localhost:8080')
// })

