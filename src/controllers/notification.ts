import { Response } from 'express'
import request from 'superagent'


const clientCredUrl: string = 'https://test-api.service.hmrc.gov.uk/oauth/token'

// OS account
// const clientId: string = 'LN3CVx3dIGhxVBwq2SK854W3ZiEV'
// const clientSecret: string = '0bfb3435-2aff-49f2-92f9-24a1c8de3eda'

// CLEAN TEST ENV
const clientId = 'LuBzF9dOCF0cGudSTh9Vkrn5OcGl'
const clientSecret = '2565b7a1-5e56-42c9-861f-dff7aa6ccad9'

// const acceptHeader: string = 'application/vnd.hmrc.1.0+json'
const boxId: string = '2741e1f7-a72c-403e-92a7-09d046e970bb' // 2741e1f7-a72c-403e-92a7-09d046e970bb = c687c23d-66ca-4ff1-aed7-49f3a5acda2a

// CUSTOM CALLS
// NESTED REQUESTS
// GET
const push = (req: any, res: Response) => {
    const getClientCredToken = request
          .post(clientCredUrl)
          .send({
            "client_secret": clientSecret,
            "client_id": clientId,
            "grant_type": "client_credentials",
            "scope": "read:pull-notifications"
          })
          .then((resp: any) => {
            const getInbox = request
                .get(`https://test-api.service.hmrc.gov.uk/misc/push-pull-notification/box/${boxId}/notifications`)
                .accept('application/vnd.hmrc.1.0+json')
                .set('Authorization', `Bearer ${resp.body.access_token}`)
                .then( (resp: any) => res.status(200).json(resp.body))
                .catch((err: any) => res.status(500).json(err.message))
          })
          .catch((err: any) => res.send(err.messaage))
}

// POST FOR CALLBACK URL VALIDATION IN HMRC
const pull = (req: any, res: Response) => {
  const { challenge } = req.query
  res.status(200).json(challenge)
}

export {push, pull}