import routeList from './routelist'
import getGmrList from './gvms/getGmrList'
import createGmr from './gvms/createGmr'
import getGmrById from './gvms/getGmrById'
import updateGmrById from './gvms/updateGmrById'
import deleteGmrById from './gvms/deleteGmrById'
import getRefData from './gvms/getRefData'
import callBack from './callback'
import {push, pull} from './notification'


export default {
    routeList,
    getGmrList,
    createGmr,
    getGmrById,
    updateGmrById,
    deleteGmrById,
    getRefData,
    callBack,
    push,
    pull
}