import { Request, Response } from 'express'
import { AccessToken } from 'simple-oauth2'
import { authorizationUri, client } from '../helpers/auth'

const routeList = (req: any, res: Response) => {
    try {
        let accessToken: AccessToken = client.createToken(req.session.oauth2Token)
        console.log(accessToken)

        const routesLocal = {
            "GET GMR LIST" : "http://localhost:8080/get-gmr-list",
            "CREATE GMR" : "http://localhost:8080/create-gmr",
            "GET GMR BY ID" : "http://localhost:8080/get-gmr-by-id/:gmrId",
            "UPDATE GMR BY ID" : "http://localhost:8080/update-gmr-by-id/:gmrId",
            "DELETE GMR BY ID" : "http://localhost:8080/delete-gmr-by-id/:gmrId",
            "GET REFERENCE DATA" : "http://localhost:8080/get-reference-data",
            "PUSH/PULL NOTIFICATION" : "http://localhost:8080/push"
        }

        const routesHeroku = {
            "GET GMR LIST" : "https://contained-server-test.herokuapp.com/get-gmr-list",
            "CREATE GMR" : "https://contained-server-test.herokuapp.com/create-gmr",
            "GET GMR BY ID" : "https://contained-server-test.herokuapp.com/get-gmr-by-id/:gmrId",
            "UPDATE GMR BY ID" : "https://contained-server-test.herokuapp.com/update-gmr-by-id/:gmrId",
            "DELETE GMR BY ID" : "https://contained-server-test.herokuapp.com/delete-gmr-by-id/:gmrId",
            "GET REFERENCE DATA" : "https://contained-server-test.herokuapp.com/get-reference-data",
            "PUSH/PULL NOTIFICATION" : "https://contained-server-test.herokuapp.com/push"
        }

        res.status(200).json({routesLocal, routesHeroku, accessToken})
    } catch (error) {
        req.session.caller = '/'
        res.redirect(authorizationUri)
    }
}

export default routeList