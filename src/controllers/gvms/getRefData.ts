import { Response } from 'express'
import callApi from "../../helpers/callApi"
import { authorizationUri, client } from '../../helpers/auth'
import { AccessToken } from 'simple-oauth2'


const getRefData = (req: any, res: Response) => {
  if (req.session.oauth2Token) {
      let accessToken: AccessToken = client.createToken(req.session.oauth2Token)
      console.log(accessToken)

      callApi({
          endpoint: '/reference-data',
          method: 'get', 
          res, 
          bearerToken: accessToken.token.access_token
      })

      console.log('GET Request at "http://localhost:8080/get-reference-data"')
  } else {
      req.session.caller = '/get-reference-data'
      res.redirect(authorizationUri)
  }
}

export default getRefData