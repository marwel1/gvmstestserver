import { Response } from 'express'
import callApi from "../../helpers/callApi"
import { authorizationUri, client } from '../../helpers/auth'
import { AccessToken } from 'simple-oauth2'


const updateGmrById = (req: any, res: Response) => {
  if (req.session.oauth2Token) {
      let accessToken: AccessToken = client.createToken(req.session.oauth2Token)
      console.log(accessToken)

      const data = {
        "direction": "GB_TO_NI",
        "isUnaccompanied": true,
        "containerReferenceNums": ["TEST123"],
        "plannedCrossing": {
          "routeId": "1",
          "localDateTimeOfDeparture": "2021-08-11T10:58"
        },
        "ataDeclarations": [
          {
            "ataCarnetId": "TestATAid"
          }
        ]
      }

      const gmrId = req.params.gmrId
      callApi({
          endpoint: `/movements/${gmrId}`,
          method: 'put', 
          res, 
          bearerToken: accessToken.token.access_token,
          data
      })

      console.log('PUT Request at "http://localhost:8080/update-gmr-by-id/:gmrId"')
  } else {
      req.session.caller = '/update-gmr-by-id'
      res.redirect(authorizationUri)
  }
}

export default updateGmrById