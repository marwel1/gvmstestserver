import { Response } from 'express'
import callApi from "../../helpers/callApi"
import { authorizationUri, client } from '../../helpers/auth'
import { AccessToken } from 'simple-oauth2'


const getGmrById = (req: any, res: Response) => {
  if (req.session.oauth2Token) {
      let accessToken: AccessToken = client.createToken(req.session.oauth2Token)
      console.log(accessToken)

      const gmrId = req.params.gmrId
      callApi({
          endpoint: `/movements/${gmrId}`,
          method: 'get', 
          res, 
          bearerToken: accessToken.token.access_token
      })

      console.log('GET Request at "http://localhost:8080/get-gmr-by-id/:gmrId"')
  } else {
      req.session.caller = '/get-gmr-by-id'
      res.redirect(authorizationUri)
  }
}

export default getGmrById