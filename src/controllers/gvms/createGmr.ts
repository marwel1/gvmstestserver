import { Response } from 'express'
import callApi from "../../helpers/callApi"
import { authorizationUri, client } from '../../helpers/auth'
import { AccessToken } from 'simple-oauth2'

const createGmr = (req: any, res: Response) => {
  if (req.session.oauth2Token) {
      let accessToken: AccessToken = client.createToken(req.session.oauth2Token)
      console.log(accessToken)

      const data = req.body

      if(data) {
        // console.log(data)
        res.status(200).json(data)

        callApi({
          endpoint: '/movements',
          method: 'post', 
          res, 
          bearerToken: accessToken.token.access_token,
          data
        })
      } else {
        // res.status(400).json({ msg: 'No data given'})

        const tempoData = {
          "direction": "UK_INBOUND",
          "isUnaccompanied": true,
          "containerReferenceNums": ["CSQU3054383"],
          "plannedCrossing": {
            "routeId": "61",
            "localDateTimeOfDeparture": "2021-08-11T10:58"
          }
        }
        
        callApi({
          endpoint: '/movements',
          method: 'post', 
          res, 
          bearerToken: accessToken.token.access_token,
          data: tempoData
        })

      }

      console.log('POST Request at "http://localhost:8080/create-gmr"')
  } else {
      req.session.caller = '/create-gmr'
      res.redirect(authorizationUri)
  }
}

export default createGmr