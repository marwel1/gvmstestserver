import { Response } from 'express'
import { client } from '../helpers/auth'
import dotenv from 'dotenv'
dotenv.config()

const callBack = async (req: any, res: Response) => {
    const { code } = req.query

    
    const redirectUri: string = 'http://localhost:8080/oauth20/callback'
    // const redirectUri: string = 'https://contained-server-test.herokuapp.com/oauth20/callback'
    
    // OS account
    // const clientId = 'LN3CVx3dIGhxVBwq2SK854W3ZiEV'
    // const clientSecret = '0bfb3435-2aff-49f2-92f9-24a1c8de3eda'
    
    // CLEAN TEST ENV
    const clientId = 'LuBzF9dOCF0cGudSTh9Vkrn5OcGl'
    const clientSecret = '2565b7a1-5e56-42c9-861f-dff7aa6ccad9'

    const options = {
      code: code,
      redirect_uri: redirectUri,
      client_id: clientId,
      client_secret: clientSecret,
    }
    
    console.log("options", options)
    try {
      const accessToken = await client.getToken(options)
      req.session.oauth2Token = accessToken

      return res.redirect(req.session.caller)
      // return res.redirect('http://localhost:3000')
    } catch (error) {
      return res.status(500).json('Authentication failed')
    }
}

export default callBack