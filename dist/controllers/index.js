"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const routelist_1 = __importDefault(require("./routelist"));
const getGmrList_1 = __importDefault(require("./getGmrList"));
const createGmr_1 = __importDefault(require("./createGmr"));
const getGmrById_1 = __importDefault(require("./getGmrById"));
const updateGmrById_1 = __importDefault(require("./updateGmrById"));
const deleteGmrById_1 = __importDefault(require("./deleteGmrById"));
const getRefData_1 = __importDefault(require("./getRefData"));
const callback_1 = __importDefault(require("./callback"));
const notification_1 = __importDefault(require("./notification"));
exports.default = {
    routeList: routelist_1.default,
    getGmrList: getGmrList_1.default,
    createGmr: createGmr_1.default,
    getGmrById: getGmrById_1.default,
    updateGmrById: updateGmrById_1.default,
    deleteGmrById: deleteGmrById_1.default,
    getRefData: getRefData_1.default,
    callBack: callback_1.default,
    push: notification_1.default
};
