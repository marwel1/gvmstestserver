"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const auth_1 = require("../helpers/auth");
const routeList = (req, res) => {
    try {
        let accessToken = auth_1.client.createToken(req.session.oauth2Token);
        console.log(accessToken);
        // const routes = {
        //     "GET GMR LIST" : "https://localhost:8080/get-gmr-list",
        //     "CREATE GMR" : "https://localhost:8080/create-gmr",
        //     "GET GMR BY ID" : "https://localhost:8080/get-gmr-by-id/:gmrId",
        //     "UPDATE GMR BY ID" : "https://localhost:8080/update-gmr-by-id/:gmrId",
        //     "DELETE GMR BY ID" : "https://localhost:8080/delete-gmr-by-id/:gmrId",
        //     "GET REFERENCE DATA" : "https://localhost:8080/get-reference-data",
        //     "PUSH/PULL NOTIFICATION" : "https://localhost:8080/push"
        // }
        const routes = {
            "GET GMR LIST": "https://contained-server-test.herokuapp.com/get-gmr-list",
            "CREATE GMR": "https://contained-server-test.herokuapp.com/create-gmr",
            "GET GMR BY ID": "https://contained-server-test.herokuapp.com/get-gmr-by-id/:gmrId",
            "UPDATE GMR BY ID": "https://contained-server-test.herokuapp.com/update-gmr-by-id/:gmrId",
            "DELETE GMR BY ID": "https://contained-server-test.herokuapp.com/delete-gmr-by-id/:gmrId",
            "GET REFERENCE DATA": "https://contained-server-test.herokuapp.com/get-reference-data",
            "PUSH/PULL NOTIFICATION": "https://contained-server-test.herokuapp.com/push"
        };
        https: //contained-server-test.herokuapp.com/
         res.status(200).json({ routes, accessToken });
    }
    catch (error) {
        req.session.caller = '/';
        res.redirect(auth_1.authorizationUri);
    }
};
exports.default = routeList;
