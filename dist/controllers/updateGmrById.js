"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const callApi_1 = __importDefault(require("../helpers/callApi"));
const auth_1 = require("../helpers/auth");
const updateGmrById = (req, res) => {
    if (req.session.oauth2Token) {
        let accessToken = auth_1.client.createToken(req.session.oauth2Token);
        console.log(accessToken);
        const data = {
            "direction": "GB_TO_NI",
            "isUnaccompanied": true,
            "containerReferenceNums": ["TEST123"],
            "plannedCrossing": {
                "routeId": "1",
                "localDateTimeOfDeparture": "2021-08-11T10:58"
            },
            "ataDeclarations": [
                {
                    "ataCarnetId": "TestATAid"
                }
            ]
        };
        const gmrId = req.params.gmrId;
        callApi_1.default({
            endpoint: `/movements/${gmrId}`,
            method: 'put',
            res,
            bearerToken: accessToken.token.access_token,
            data
        });
        console.log('PUT Request at "http://localhost:8080/update-gmr-by-id/:gmrId"');
    }
    else {
        req.session.caller = '/update-gmr-by-id';
        res.redirect(auth_1.authorizationUri);
    }
};
exports.default = updateGmrById;
