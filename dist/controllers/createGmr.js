"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const callApi_1 = __importDefault(require("../helpers/callApi"));
const auth_1 = require("../helpers/auth");
const createGmr = (req, res) => {
    if (req.session.oauth2Token) {
        let accessToken = auth_1.client.createToken(req.session.oauth2Token);
        console.log(accessToken);
        const data = {
            "direction": "GB_TO_NI",
            "isUnaccompanied": true,
            "vehicleRegNum": "TSN 123",
            "plannedCrossing": {
                "routeId": "1",
                "localDateTimeOfDeparture": "2021-08-11T10:58"
            },
            "customsDeclarations": [
                {
                    "customsDeclarationId": "0GB689223596000-SE119404",
                    "sAndSMasterRefNum": "20GB01I0XLM976S001"
                }, {
                    "customsDeclarationId": "0GB689223596000-SE119405",
                    "sAndSMasterRefNum": "20GB01I0XLM976S002"
                }
            ],
            "transitDeclarations": [
                {
                    "transitDeclarationId": "10GB00002910B75BE5",
                    "isTSAD": true
                }, {
                    "transitDeclarationId": "10GB00002910B75BE6",
                    "sAndSMasterRefNum": "20GB01I0XLM976S004",
                    "isTSAD": false
                }
            ]
        };
        callApi_1.default({
            endpoint: '/movements',
            method: 'post',
            res,
            bearerToken: accessToken.token.access_token,
            data
        });
        console.log('POST Request at "http://localhost:8080/create-gmr"');
    }
    else {
        req.session.caller = '/create-gmr';
        res.redirect(auth_1.authorizationUri);
    }
};
exports.default = createGmr;
