"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const callApi_1 = __importDefault(require("../helpers/callApi"));
const auth_1 = require("../helpers/auth");
const getGmrList = (req, res) => {
    if (req.session.oauth2Token) {
        let accessToken = auth_1.client.createToken(req.session.oauth2Token);
        console.log(accessToken);
        callApi_1.default({
            endpoint: '/movements',
            method: 'get',
            res,
            bearerToken: accessToken.token.access_token
        });
        console.log('GET Request at "http://localhost:8080/get-gmr-list"');
    }
    else {
        req.session.caller = '/get-gmr-list';
        res.redirect(auth_1.authorizationUri);
    }
};
exports.default = getGmrList;
