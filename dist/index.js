"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const cookie_session_1 = __importDefault(require("cookie-session"));
const cors_1 = __importDefault(require("cors"));
const movements_1 = __importDefault(require("./routes/movements"));
const PORT = process.env.PORT || 8080;
const app = express_1.default();
app.use(cookie_session_1.default({
    name: 'session',
    keys: ['oauth2Token', 'caller'],
    maxAge: 5 * 60 * 60 * 1000, // 5 hours
}));
app.use(cors_1.default());
app.use('/', movements_1.default);
app.listen(PORT, () => {
    console.log('Started at http://localhost:8080');
});
// TEST HTTPS
// import https from 'https'
// import fs from 'fs'
// const server = https.createServer({
//   key: fs.readFileSync('server.key'),
//   cert: fs.readFileSync('server.cert')
// }, app)
// server.listen(PORT, () => {
//   console.log('Started at https://localhost:8080')
// })
