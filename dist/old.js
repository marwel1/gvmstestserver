"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const simple_oauth2_1 = require("simple-oauth2");
const superagent_1 = __importDefault(require("superagent"));
const cookie_session_1 = __importDefault(require("cookie-session"));
const dotenv_1 = __importDefault(require("dotenv"));
dotenv_1.default.config();
const clientId = process.env.CLIENT_ID;
const clientSecret = process.env.CLIENT_SECRET;
const serverToken = process.env.SERVER_TOKEN;
const gvmsBaseUrl = 'https://test-api.service.hmrc.gov.uk/';
const serviceName = 'customs/goods-movement-system';
const serviceVersion = '1.0';
const oauthScope = 'write:goods-movement-system';
const redirectUri = 'http://localhost:8080/oauth20/callback';
// const redirectUri = 'https://test-gvms.herokuapp.com/oauth20/callback'
const PORT = process.env.PORT || 8080;
const app = express_1.default();
app.use(cookie_session_1.default({
    name: 'session',
    keys: ['oauth2Token', 'caller'],
    maxAge: 5 * 60 * 60 * 1000, // 5 hours
}));
const client = new simple_oauth2_1.AuthorizationCode({
    client: {
        id: clientId,
        secret: clientSecret,
    },
    auth: {
        tokenHost: gvmsBaseUrl,
        tokenPath: '/oauth/token',
        authorizePath: '/oauth/authorize',
    },
});
const authorizationUri = client.authorizeURL({
    redirect_uri: redirectUri,
    scope: oauthScope,
});
app.get('/', (req, res) => {
    try {
        let accessToken = client.createToken(req.session.oauth2Token);
        console.log(accessToken);
        const routes = {
            "GET GMR LIST": "http://localhost:8080/get-GMR-list",
            "CREATE GMR": "http://localhost:8080/create-GMR",
            "GET GMR BY ID": "http://localhost:8080/get-gmr-by-id/:gmrId",
            "UPDATE GMR BY ID": "http://localhost:8080/update-gmr-by-id/:gmrId",
            "DELETE GMR BY ID": "http://localhost:8080/delete-gmr-by-id/:gmrId",
            "GET REFERENCE DATA": "http://localhost:8080/get-reference-data"
        };
        res.status(200).json({ routes, accessToken });
    }
    catch (error) {
        req.session.caller = '/';
        res.redirect(authorizationUri);
    }
});
// GET GMR LIST
app.get('/get-GMR-list', (req, res) => {
    if (req.session.oauth2Token) {
        let accessToken = client.createToken(req.session.oauth2Token);
        console.log(accessToken);
        callApi({
            endpoint: '/movements',
            method: 'get',
            res,
            bearerToken: accessToken.token.access_token
        });
        console.log('GET Request at "http://localhost:8080/get-GMR-list"');
    }
    else {
        req.session.caller = '/get-GMR-list';
        res.redirect(authorizationUri);
    }
});
// CREATE GMR
app.get('/create-GMR', (req, res) => {
    if (req.session.oauth2Token) {
        let accessToken = client.createToken(req.session.oauth2Token);
        console.log(accessToken);
        const data = {
            "direction": "GB_TO_NI",
            "isUnaccompanied": false,
            "vehicleRegNum": "TEST DEF",
            "plannedCrossing": {
                "routeId": "1",
                "localDateTimeOfDeparture": "2021-08-11T10:58"
            },
            "customsDeclarations": [
                {
                    "customsDeclarationId": "0GB689223596000-SE119404",
                    "sAndSMasterRefNum": "20GB01I0XLM976S001"
                }, {
                    "customsDeclarationId": "0GB689223596000-SE119405",
                    "sAndSMasterRefNum": "20GB01I0XLM976S002"
                }
            ],
            "transitDeclarations": [
                {
                    "transitDeclarationId": "10GB00002910B75BE5",
                    "isTSAD": true
                }, {
                    "transitDeclarationId": "10GB00002910B75BE6",
                    "sAndSMasterRefNum": "20GB01I0XLM976S004",
                    "isTSAD": false
                }
            ]
        };
        callApi({
            endpoint: '/movements',
            method: 'post',
            res,
            bearerToken: accessToken.token.access_token,
            data: data
        });
        console.log('POST Request at "http://localhost:8080/create-GMR"');
    }
    else {
        req.session.caller = '/create-GMR';
        res.redirect(authorizationUri);
    }
});
// GET GMR BY ID
app.get('/get-gmr-by-id/:gmrId', (req, res) => {
    if (req.session.oauth2Token) {
        let accessToken = client.createToken(req.session.oauth2Token);
        console.log(accessToken);
        const gmrId = req.params.gmrId;
        callApi({
            endpoint: `/movements/${gmrId}`,
            method: 'get',
            res,
            bearerToken: accessToken.token.access_token,
        });
        console.log('POST Request at "http://localhost:8080/get-gmr-by-id/:gmrId"');
    }
    else {
        req.session.caller = '/get-gmr-by-id/:gmrId';
        res.redirect(authorizationUri);
    }
});
// UPDATE GMR BY ID
app.get('/update-gmr-by-id/:gmrId', (req, res) => {
    if (req.session.oauth2Token) {
        let accessToken = client.createToken(req.session.oauth2Token);
        console.log(accessToken);
        const data = {
            direction: "GB_TO_NI",
            isUnaccompanied: true,
            containerReferenceNums: ["CCC123"],
            plannedCrossing: {
                routeId: "1",
                localDateTimeOfDeparture: "2021-08-11T10:58"
            },
            ataDeclarations: [
                {
                    ataCarnetId: "TestATAid"
                }
            ]
        };
        const gmrId = req.params.gmrId;
        callApi({
            endpoint: `/movements/${gmrId}`,
            method: 'put',
            res,
            bearerToken: accessToken.token.access_token,
            data: data
        });
        console.log('UPDATE Request at "http://localhost:8080/update-gmr-by-id/:gmrId"');
    }
    else {
        req.session.caller = '/update-gmr-by-id/:gmrId';
        res.redirect(authorizationUri);
    }
});
// DELETE GMR BY ID
app.get('/delete-gmr-by-id/:gmrId', (req, res) => {
    if (req.session.oauth2Token) {
        let accessToken = client.createToken(req.session.oauth2Token);
        console.log(accessToken);
        const gmrId = req.params.gmrId;
        callApi({
            endpoint: `/movements/${gmrId}`,
            method: 'delete',
            res,
            bearerToken: accessToken.token.access_token,
        });
        console.log('DELETE Request at "http://localhost:8080/delete-gmr-by-id/:gmrId"');
    }
    else {
        req.session.caller = '/delete-gmr-by-id/:gmrId';
        res.redirect(authorizationUri);
    }
});
// GET REFERENCE DATA
app.get('/get-reference-data', (req, res) => {
    if (req.session.oauth2Token) {
        let accessToken = client.createToken(req.session.oauth2Token);
        console.log(accessToken);
        callApi({
            endpoint: '/reference-data',
            method: 'get',
            res,
            bearerToken: accessToken.token.access_token
        });
        console.log('GET Request at "http://localhost:8080/get-reference-data"');
    }
    else {
        req.session.caller = '/get-reference-data';
        res.redirect(authorizationUri);
    }
});
app.get('/oauth20/callback', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { code } = req.query;
    const options = {
        code: code,
        redirect_uri: redirectUri,
        client_id: clientId,
        client_secret: clientSecret,
    };
    try {
        const accessToken = yield client.getToken(options);
        req.session.oauth2Token = accessToken;
        return res.redirect(req.session.caller);
    }
    catch (error) {
        return res.status(500).json('Authentication failed');
    }
}));
// endpoint: any, res: any, bearerToken: any, method = 'get', data: any
const callApi = (params) => {
    const acceptHeader = `application/vnd.hmrc.${serviceVersion}+json`;
    const url = gvmsBaseUrl + serviceName + params.endpoint;
    // @ts-ignore
    const req = superagent_1.default[params.method](url).accept(acceptHeader);
    if (params.bearerToken) {
        req.set('Authorization', `Bearer ${params.bearerToken}`);
    }
    if (params.method === 'post' || params.method === 'put' && params.data !== undefined) {
        req.send(params.data);
    }
    req.end((err, apiResponse) => handleResponse(params.res, err, apiResponse));
};
function handleResponse(res, err, apiResponse) {
    if (err || !apiResponse.ok) {
        res.send(err);
    }
    else {
        res.send(apiResponse.body);
    }
}
app.listen(PORT, () => {
    console.log('Started at http://localhost:8080');
});
