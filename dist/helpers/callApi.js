"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const superagent_1 = __importDefault(require("superagent"));
const handleResponse_1 = __importDefault(require("./handleResponse"));
const gvmsBaseUrl = 'https://test-api.service.hmrc.gov.uk/';
const serviceName = 'customs/goods-movement-system';
const serviceVersion = '1.0';
// endpoint: any, res: any, bearerToken: any, method = 'get', data: any
const callApi = (params) => {
    const acceptHeader = `application/vnd.hmrc.${serviceVersion}+json`;
    const url = gvmsBaseUrl + serviceName + params.endpoint;
    // @ts-ignore
    const req = superagent_1.default[params.method](url).accept(acceptHeader);
    if (params.bearerToken) {
        req.set('Authorization', `Bearer ${params.bearerToken}`);
    }
    if (params.method === 'post' || params.method === 'put' && params.data !== undefined) {
        req.send(params.data);
    }
    req.end((err, apiResponse) => handleResponse_1.default(params.res, err, apiResponse));
};
exports.default = callApi;
