"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const handleResponse = (res, err, apiResponse) => {
    if (err || !apiResponse.ok) {
        res.send(err);
    }
    else {
        res.send(apiResponse.body);
    }
};
exports.default = handleResponse;
