"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.authorizationUri = exports.client = void 0;
const simple_oauth2_1 = require("simple-oauth2");
const dotenv_1 = __importDefault(require("dotenv"));
dotenv_1.default.config();
const gvmsBaseUrl = 'https://test-api.service.hmrc.gov.uk/';
// const redirectUri: string = 'https://localhost:8080/oauth20/callback'
const redirectUri = 'https://contained-server-test.herokuapp.com/oauth20/callback';
const oauthScope = 'write:goods-movement-system';
const clientId = 'LN3CVx3dIGhxVBwq2SK854W3ZiEV';
const clientSecret = '0bfb3435-2aff-49f2-92f9-24a1c8de3eda';
const client = new simple_oauth2_1.AuthorizationCode({
    client: {
        id: clientId,
        secret: clientSecret,
    },
    auth: {
        tokenHost: gvmsBaseUrl,
        tokenPath: '/oauth/token',
        authorizePath: '/oauth/authorize',
    },
});
exports.client = client;
const authorizationUri = client.authorizeURL({
    redirect_uri: redirectUri,
    scope: oauthScope,
});
exports.authorizationUri = authorizationUri;
