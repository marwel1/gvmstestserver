"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const controllers_1 = __importDefault(require("../controllers"));
const movementsRouter = express_1.default.Router();
movementsRouter.get('/', controllers_1.default.routeList);
movementsRouter.get('/get-gmr-list', controllers_1.default.getGmrList);
movementsRouter.get('/create-gmr', controllers_1.default.createGmr);
movementsRouter.get('/get-gmr-by-id/:gmrId', controllers_1.default.getGmrById);
movementsRouter.get('/update-gmr-by-id/:gmrId', controllers_1.default.updateGmrById);
movementsRouter.get('/delete-gmr-by-id/:gmrId', controllers_1.default.deleteGmrById);
movementsRouter.get('/get-reference-data', controllers_1.default.getRefData);
movementsRouter.get('/oauth20/callback', controllers_1.default.callBack);
movementsRouter.get('/push', controllers_1.default.push);
exports.default = movementsRouter;
